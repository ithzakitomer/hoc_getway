var express = require('express');
var router = express.Router();
const { operationDataDict, operationGroupsDict, controllingSites, telegrams, events, steps } = require('../mocks/operations.js');
/* GET home page. */

router.get('/operations', function (req, res) {
  res.status(200).send({ operationGroupsDict, operationDataDict })
});

router.get('/telegrams/:id', (req, res) => {
  res.status(200).send(telegrams)
})


router.get('/events/:id', (req, res) => {
  res.status(200).send(events)
})


router.get('/operations/:id/steps', (req, res) => {
  res.status(200).send(steps)
})

router.get('/control/outposts/list', (req, res) => {
  res.status(200).send(controllingSites)
})//s


module.exports = router;
