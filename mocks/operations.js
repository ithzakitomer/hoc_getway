
const controllingSites = [
    { id: 20, name: 'בננות' },
    { id: 30, name: 'עגבניות' },
    { id: 40, name: 'אגוזים' }
]

const operationGroupsDict = {
    20: ['1', '2'],
    30: ['3', '4', '5'],
    40: ['6', '7']
}

const operationDataDict = {
    '1': {
        id: 1,
        name: 'דולפינים כחולים',
        start: '2021-10-25T21:00:00.00Z',
        end: '2021-10-27T21:00:00.00Z',
        shin: '2021-10-28T21:00:00.00Z'
    },
    '2': {
        id: 2,
        name: 'דולפינים ירוקים',
        start: '2021-10-25T21:00:00.00Z',
        end: '2021-10-27T21:00:00.00Z',
        shin: '2021-10-28T21:00:00.00Z'
    },
    '3': {
        id: 3,
        name: 'אריות שחורים',
        start: '2021-10-25T21:00:00.00Z',
        end: '2021-10-27T21:00:00.00Z',
        shin: '2021-10-28T21:00:00.00Z'
    },
    '4': {
        id: 4,
        name: 'אריות צהובים',
        start: '2021-10-25T21:00:00.00Z',
        end: '2021-10-27T21:00:00.00Z',
        shin: '2021-10-28T21:00:00.00Z'
    },
    '5': {
        id: 5,
        name: 'אריות ירוקים',
        start: '2021-10-25T21:00:00.00Z',
        end: '2021-10-27T21:00:00.00Z',
        shin: '2021-10-28T21:00:00.00Z'
    },
    '6': {
        id: 6,
        name: 'סוסים אדומים',
        start: '2021-10-25T21:00:00.00Z',
        end: '2021-10-27T21:00:00.00Z',
        shin: '2021-10-28T21:00:00.00Z'
    },
    '7': {
        id: 7,
        name: 'סוסים לבנים',
        start: '2021-10-25T21:00:00.00Z',
        end: '2021-10-27T21:00:00.00Z',
        shin: '2021-10-28T21:00:00.00Z'
    }
}

const telegrams = [
    {
        id: '1',
        subject: 'לך לישון',
        broadcastTime: '2022-05-29T08:02:51.920Z',
        outpost: 'מוצב תותים',
        sender: 'תומר',
        type: "out",
        nerLabel: 'או"ם',
        ner: '1150505050'
    },
    {
        id: '2',
        subject: 'נשר לדבורה קיבלתי',
        broadcastTime: '2022-06-30T09:02:51.920Z',
        outpost: 'מוצב אגוזים',
        sender: 'רוני',
        type: "in",
        nerLabel: 'או"ם',
        ner: '8383881898'
    },
]

const steps = [
    {
        StepID: '3',
        StepContent: 'סלק',
        StepCallingSignal: 'י',//מילת דיווח
        // shinTimeDelta: 4,
        // OperationID:1,
        StepPlannedTime: '2022-06-20T09:02:51.920Z',//זמן מתוכנן
        StepExecutionTime: '2022-06-21T09:02:51.920Z',//זמן ביצוע
        StepStatus:0,
        StepRemarks: 'גגג',//פירוט סודר
        //פירוט חריגה
        // type: 0
    },
    {
        StepID: '4',
        // OperationID:2,
        StepContent: 'גזר',
        StepCallingSignal: 'נ',//מילת דיווח
        // shinTimeDelta: 4,
        StepPlannedTime: '2022-06-20T09:02:51.920Z',//זמן מתוכנן
        StepExecutionTime: '2022-06-21T09:02:51.920Z',//זמן ביצוע
        StepRemarks: 'דדד',//פירוט סודר
        StepStatus:1
        //פירוט חריגה
        // type: 0
    }
]

const events = [
    {
        Reports: [
            {
                ID: '8',
                Name: 'גזר הולך',
                ReporterCenter: 'בננות',
                ReportedBy: 'עידן',
                Created: '/Date(1655028445000)/'
            },
            {
                ID: '9',
                Name: 'עגבנייה נשארת',
                ReporterCenter: 'תפוזים',
                ReportedBy: 'רוני',
                Created: '/Date(1655028445000)/'
            }
        ]
    }]


module.exports = { operationGroupsDict, controllingSites, operationDataDict, events, steps, telegrams }